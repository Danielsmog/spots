//
//  DetailViewController.h
//  LisbonSpots
//
//  Created by Daniel Gomes on 10/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Spot.h"

@interface DetailViewController : UIViewController
@property Spot *spot;

@end

