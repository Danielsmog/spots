//
//  DetailTableViewController.m
//  LisbonSpots
//
//  Created by Daniel Gomes on 14/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import "DetailTableViewController.h"
#import <MapKit/MapKit.h>
#import "TableViewController.h"

@interface DetailTableViewController () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *spotName;
@property (weak, nonatomic) IBOutlet UILabel *spotType;
@property (weak, nonatomic) IBOutlet UITextView *spotPhone;
@property (weak, nonatomic) IBOutlet UITextView *spotDesc;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _spotName.text = _spot.spotName;
    _spotType.text = _spot.spotType;
    _spotDesc.text = _spot.spotDesc;
    _spotPhone.text = _spot.spotPhone;
    
    _mapView.delegate = self;
    
    [_mapView addAnnotation:_spot];
    
    MKCoordinateRegion spotLoc = MKCoordinateRegionMake(_spot.coordinate, MKCoordinateSpanMake(0.01, 0.01));
    
    [_mapView setRegion: spotLoc animated:YES];
    
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - MKAnnotation methods

// change custom pins

//- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation {
//
//    if([annotation isKindOfClass: [MKUserLocation class]]) {
//        return nil;
//    }
//
//    MKAnnotationView *view = [[MKAnnotationView alloc]
//                              initWithAnnotation:annotation reuseIdentifier:@"pin"];
//
//    if (view == nil) {
//        view = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"spots"];
//    } else {
//        view.annotation = annotation;
//    }
//
//    view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//    view.image =[UIImage imageNamed:@"paper-clip.png"];
//
//    [view setEnabled:YES];
//    [view setCanShowCallout:YES];
//    return view;
//
//}


@end

