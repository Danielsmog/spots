//
//  ClubTableViewCell.h
//  LisbonSpots
//
//  Created by Daniel Gomes on 12/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClubTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelClub;


@end

