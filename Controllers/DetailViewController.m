//
//  DetailViewController.m
//  LisbonSpots
//
//  Created by Daniel Gomes on 10/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import "DetailViewController.h"
#import <MapKit/MapKit.h>
#import "MapViewController.h"


@interface DetailViewController () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *spotName;
@property (weak, nonatomic) IBOutlet UILabel *spotType;
@property (weak, nonatomic) IBOutlet UITextView *spotPhone;
@property (weak, nonatomic) IBOutlet UITextView *spotDesc;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _spotName.text = _spot.spotName;
    _spotType.text = _spot.spotType;
    _spotDesc.text = _spot.spotDesc;
    _spotPhone.text = _spot.spotPhone;
    
    _mapView.delegate = self;
    
    [_mapView addAnnotation:_spot];
    
    MKCoordinateRegion spotCenter = MKCoordinateRegionMake(_spot.coordinate, MKCoordinateSpanMake(0.3, 0.3));
    
    [_mapView setRegion: spotCenter animated:YES];
    
    }


- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)receiverNotification:(NSNotification*)notification{
    
}
#pragma mark - MKAnnotation methods


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if([annotation isKindOfClass: [MKUserLocation class]]) {
        return nil;
    }
    
    MKAnnotationView *view = [[MKAnnotationView alloc]
                              initWithAnnotation:annotation reuseIdentifier:@"pin"];
    
    if (view == nil) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"spots"];
    } else {
        view.annotation = annotation;
    }
    
    view.image =[UIImage imageNamed:@"paper-clip.png"];
    
    [view setEnabled:YES];
    [view setCanShowCallout:YES];
    
    return view;
    
}


@end
