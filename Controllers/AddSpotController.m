//
//  AddSpotController.m
//  LisbonSpots
//
//  Created by Daniel Gomes on 15/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import "AddSpotController.h"
#import "TableViewController.h"
#import <MapKit/MapKit.h>

@interface AddSpotController () <UITextFieldDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nSname;
@property (weak, nonatomic) IBOutlet UITextField *nSphone;
@property (weak, nonatomic) IBOutlet UITextField *nStype;
@property (weak, nonatomic) IBOutlet UITextView *nSdesc;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation AddSpotController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mapView.delegate = self;
    
}

- (IBAction)saveButton:(id)sender {
    
    NSDictionary *newSpots    = @{@"type": _nStype.text,
                                     @"name": _nSname.text,
                                     @"phone": _nSphone.text,
                                     @"desc": _nSdesc.text,
                                     @"latitude": [NSString stringWithFormat:@"%f", _mapView.centerCoordinate.latitude],
                                     @"longitude":[NSString stringWithFormat:@"%f", _mapView.centerCoordinate.longitude]};
    
    [Spot spotWithDictionary: newSpots];
    
    TableViewController *gotoTableView = [self.storyboard
                                          instantiateViewControllerWithIdentifier:@"TableViewController"];
    
    [self.navigationController pushViewController:gotoTableView animated:YES];
}


- (void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    MKCoordinateRegion newRegion = MKCoordinateRegionMake(_mapView.userLocation.coordinate,
                                                          MKCoordinateSpanMake(0.05, 0.05));
    
    [_mapView setRegion: newRegion animated:NO];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
