//
//  MapViewController.m
//  LisbonSpots
//
//  Created by Admin on 13/09/2017.
//  Copyright © 2017 Daniel. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "Spot.h"
#import "DetailViewController.h"


@interface MapViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property CLLocationManager *locationManager;
@property Spot *spotDetail;

@end


@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestAlwaysAuthorization];
    
    _map.delegate = self;
    
    NSArray *spots = [Spot allSpots];
    NSLog(@"%@", spots);
    
    [_map addAnnotations:spots];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"spot_received"
                                               object:nil];
}

- (void)receiveNotification:(NSNotification*)notification {
    NSArray *spots = [Spot allSpots];
    NSLog(@"%@", spots);
    
    [_map addAnnotations:spots];
}

- (IBAction)slidereChanged:(UISlider *)sender {
    // con distancia
    //MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(_map.userLocation.coordinate, sender.value * 1000, sender.value * 1000);
    
    MKCoordinateRegion newRegion = MKCoordinateRegionMake(_map.userLocation.coordinate,
    MKCoordinateSpanMake(sender.value, sender.value));
    
    [_map setRegion: newRegion animated:NO];
    
}

- (IBAction)toggleLocation:(UISwitch *)sender {
    _map.showsUserLocation = sender.isOn;

}

- (IBAction)changeMapType:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            _map.mapType = MKMapTypeStandard;
            break;
        case 1:
            _map.mapType = MKMapTypeHybrid;
            break;
        case 2:
            _map.mapType = MKMapTypeSatellite;
            break;
        default:
            break;
            
    }
}

# pragma mark - MKMapViewDelegate methods

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    _map.centerCoordinate = userLocation.location.coordinate;
    _slider.value = mapView.region.span.latitudeDelta;
    
}

//#pragma mark - MKAnnotation methods

//change custom pins

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation {

    if([annotation isKindOfClass: [MKUserLocation class]]) {
        return nil;
    }

    MKAnnotationView *view = [[MKAnnotationView alloc]
                              initWithAnnotation:annotation reuseIdentifier:@"pin"];

    if (view == nil) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"spots"];
    } else {
        view.annotation = annotation;
    }

    view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    view.image =[UIImage imageNamed:@"paper-clip.png"];

    [view setEnabled:YES];
    [view setCanShowCallout:YES];
    return view;

}

//deails

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {

    _spotDetail = view.annotation;
    [self performSegueWithIdentifier:@"mapViewDetail" sender:self];

}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.destinationViewController isKindOfClass: [DetailViewController class]]) {
        DetailViewController *target = segue.destinationViewController;

        target.spot = _spotDetail;
    }
}



@end
