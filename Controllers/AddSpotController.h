//
//  AddSpotController.h
//  LisbonSpots
//
//  Created by Daniel Gomes on 15/10/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Spot.h"

@interface AddSpotController : UIViewController

@property Spot *spotAll;

@end
