//
//  AppDelegate.h
//  LisbonSpots
//
//  Created by Admin on 27/09/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "JsonConsumer.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, JsonConsumer>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

+ (AppDelegate*)sharedDelegate;


@end

