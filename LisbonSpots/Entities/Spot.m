//
//  spot.m
//  MapKit2
//
//  Created by Admin on 13/09/2017.
//  Copyright © 2017 Daniel. All rights reserved.
//

#import "Spot.h"
#import "AppDelegate.h"
#import "JsonConsumer.h"


@implementation Spot

@dynamic spotName;
@dynamic spotType;
@dynamic spotLatitude;
@dynamic spotLongitude;
@dynamic spotIdentification;
@dynamic spotDesc;
@dynamic spotDateCreated;
@dynamic spotPhone;


+(instancetype)spotWithName:(NSString * ) name coord:(CLLocationCoordinate2D)coord {

    NSPersistentContainer *persistentContainer = [AppDelegate sharedDelegate].persistentContainer;
    NSManagedObjectContext *moc = persistentContainer.viewContext;
    
    Spot *spot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot"
                                               inManagedObjectContext:moc];
    
    spot.spotName = name;
    spot.spotLatitude = coord.latitude;
    spot.spotLongitude = coord.longitude;
    spot.spotDateCreated = [[NSDate date] timeIntervalSince1970];

    [[AppDelegate sharedDelegate] saveContext];
    
    return spot;
}


+ (instancetype)spotWithDictionary:(NSDictionary *)dict {
    
    NSPersistentContainer *persistentContainer = [AppDelegate sharedDelegate].persistentContainer;
    NSManagedObjectContext *moc = persistentContainer.viewContext;
    
    Spot *spots = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:moc];
    
    //spot - properties
    
  
        spots.spotIdentification = dict[@"id"];
        spots.spotType = dict[@"type"];
        spots.spotPhone = dict[@"phone"];
        spots.spotDesc = dict[@"desc"];
        spots.spotName = dict [@"name"];
        spots.spotLatitude = [dict [@"latitude"] doubleValue];
        spots.spotLongitude = [dict [@"longitude"] doubleValue];
        spots.spotDateCreated = [[NSDate date] timeIntervalSince1970];
    
    [[AppDelegate sharedDelegate] saveContext];
    
    return spots;
}




+ (NSArray*)allSpots{

    //AppDelegate *appDelegate = [AppDelegate sharedDelegate];
    
    NSPersistentContainer *persistentContainer = [AppDelegate sharedDelegate].persistentContainer;
    NSManagedObjectContext *moc = persistentContainer.viewContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    NSError *error;
    NSArray *result = [moc executeFetchRequest:request error:&error];
    
    if(error != nil ){
        NSLog(@"[Spot allSpots] -> %@", error);
        return nil;
        }
    NSLog(@"[Spot allLisbonSpots] -> %@", result);

    return result;
    
}

#pragma Mark - MKAnnotation methods
- (CLLocationCoordinate2D)coordinate{
    return  CLLocationCoordinate2DMake(self.spotLatitude, self.spotLongitude);
}

-(NSString *)title {
    return self.spotName;

}

- (NSString*) description {
    
    return [NSString stringWithFormat:@"%@, %@, %@, %@, %f, %f", self.spotName, self.spotPhone, self.spotType, self.spotDesc, self.spotLatitude, self.spotLongitude];
}

@end
