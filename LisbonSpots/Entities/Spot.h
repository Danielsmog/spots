//
//  spot.h
//  MapKit2
//
//  Created by Admin on 13/09/2017.
//  Copyright © 2017 Daniel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>


@interface Spot : NSManagedObject <MKAnnotation>


@property (nonatomic) NSString *spotName;
@property (nonatomic) NSString *spotPhone;
@property (nonatomic) NSNumber *spotIdentification;
@property (nonatomic) NSString *spotType;
@property (nonatomic) NSString *spotDesc;
@property (nonatomic) double spotLatitude;
@property (nonatomic) double spotLongitude;
@property (nonatomic) NSTimeInterval spotDateCreated;



+ (instancetype)spotWithName:(NSString *)name coord:(CLLocationCoordinate2D)coord;
+ (instancetype)spotWithDictionary:(NSDictionary *)dict;
+ (NSArray*)allSpots;

-(NSString*)title;
-(CLLocationCoordinate2D)coordinate;


@end

