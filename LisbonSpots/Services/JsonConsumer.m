//
//  JsonConsumer.m
//  LisbonSpots
//
//  Created by daniel gomes on 28/09/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import "JsonConsumer.h"
#import "Spot.h"

@implementation JsonConsumer

+ (NSURL *)jsonURL {
    return [NSURL URLWithString:@"http://spots.franciscocosta.net"];
}

+(void)jsonSpotsWithCallback :(id<JsonConsumer>)invoker {
    NSURL *url = [JsonConsumer jsonURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
        completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError *error) {
            
            if (error != nil){
                NSLog(@"Response:%@", response);
                NSLog(@"Error: %@", error);
                
                return;
            }
            NSArray *spots = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"%@", spots);

            dispatch_async(dispatch_get_main_queue(), ^{
                if ([invoker respondsToSelector:@selector(receiveJson:)]){
                    [invoker receiveJson:spots];
                }
            });
        }];
    [task resume];
}


@end
