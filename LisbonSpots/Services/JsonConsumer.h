//
//  JsonConsumer.h
//  LisbonSpots
//
//  Created by daniel gomes on 28/09/2017.
//  Copyright © 2017 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>



@protocol JsonConsumer <NSObject>

@optional
- (void)receiveJson:(NSArray*)result;

@end

@interface JsonConsumer : NSObject

+(NSURL*)jsonURL;
+(void)jsonSpotsWithCallback:(id<JsonConsumer>)invoker;

@end
